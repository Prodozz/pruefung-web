package com.example.demo.DTOs;

import com.example.demo.entities.ContractEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;



@Getter
@AllArgsConstructor
public class ArtistDTO {

    private int artistId;
    private String formationDate;
    private String name;
    private String genre;
    private String socialMediaUrl;
    private List<Integer> contractEntitiesIds;


}
