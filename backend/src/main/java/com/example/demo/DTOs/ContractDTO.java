package com.example.demo.DTOs;

import com.example.demo.entities.ArtistEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ContractDTO {

    private int contractId;
    private String startDate;
    private String endDate;
    private int salaryPerThousandViews;
    private Integer artistId;


}
