package com.example.demo.controllers;


import com.example.demo.DTOs.ArtistDTO;
import com.example.demo.repositories.ArtistCRUDRepository;
import com.example.demo.services.ArtistService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
@RequestMapping(path = "/api")
@AllArgsConstructor
public class ArtistController {

    final ArtistService artistService;
    final ArtistCRUDRepository artistCRUDRepository;

    @GetMapping("/artists")
    public ResponseEntity<?> getAllArtists() {
        try {
            return ResponseEntity.ok(artistService.getAllArtists());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/artists/{artistId}")
    public ResponseEntity<?> getArtist(@PathVariable int artistId) {
        try {
            return ResponseEntity.ok(artistService.getArtist(artistId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/artists")
    public ResponseEntity<?> createArtist(@RequestBody ArtistDTO artistDTO) {
        try {
            artistService.createArtist(artistDTO);
            return ResponseEntity.ok(artistService.getAllArtists());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/artists/{artistId}")
    public ResponseEntity<?> deleteArtist(@PathVariable int artistId) {
        try {
            artistService.deleteArtist(artistId);
            return ResponseEntity.ok(artistService.getAllArtists());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/artists")
    public ResponseEntity<?> updateArtist(@RequestBody ArtistDTO artistDTO) {
        try {
            artistService.updateArtist(artistDTO);
            return ResponseEntity.ok(artistService.getArtist(artistDTO.getArtistId()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}



