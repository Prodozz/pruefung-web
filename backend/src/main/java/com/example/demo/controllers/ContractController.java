package com.example.demo.controllers;

import com.example.demo.DTOs.ContractDTO;
import com.example.demo.repositories.ContractCRUDRepository;
import com.example.demo.services.ContractService;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
@RequestMapping(path = "/api")
@AllArgsConstructor
public class ContractController {
    final ContractService contractService;
    final ContractCRUDRepository contractCRUDRepository;

    @GetMapping("/contracts")
    public ResponseEntity<?> getAllContracts() {
        try {
            return ResponseEntity.ok(contractService.getAllContracts());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/contracts/{contractId}")
    public ResponseEntity<?> getContract(@PathVariable int contractId) {
        try {
            return ResponseEntity.ok(contractService.getContract(contractId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/contracts")
    public ResponseEntity<?> createContract(@RequestBody ContractDTO contractDTO) {
        try {
            contractService.createContract(contractDTO);
            return ResponseEntity.ok(contractService.getAllContracts());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/contracts/{contractId}")
    public ResponseEntity<?> deleteContract(@PathVariable int contractId) {
        try {
            contractService.deleteContract(contractId);
            return ResponseEntity.ok(contractService.getAllContracts());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/contracts")
    public ResponseEntity<?> updateContract(@RequestBody ContractDTO contractDTO) {
        try {
            contractService.updateContract(contractDTO);
            return ResponseEntity.ok(contractService.getContract(contractDTO.getContractId()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

}
