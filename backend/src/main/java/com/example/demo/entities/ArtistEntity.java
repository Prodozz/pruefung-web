package com.example.demo.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ArtistEntity {

    @Id
    @GeneratedValue
    private int artistId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String formationDate;
    private String name;
    private String genre;
    private String socialMediaUrl;

    @OneToMany(mappedBy = "artist")
    private List<ContractEntity> contractEntities = new ArrayList<>();


}
