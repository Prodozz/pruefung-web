package com.example.demo.mappers;

import com.example.demo.DTOs.ArtistDTO;
import com.example.demo.entities.ArtistEntity;
import com.example.demo.entities.ContractEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface ArtistMapper {

    ArtistMapper ARTIST_MAPPER = Mappers.getMapper(ArtistMapper.class);

    @Mapping(source = "contractEntities", target = "contractEntitiesIds", qualifiedByName = "contractEntitiesToIntegers")
    ArtistDTO toDto(ArtistEntity artistEntity);


    @Mapping(target = "contractEntities", ignore = true)
    ArtistEntity toEntity(ArtistDTO artistDTO);

    @Mapping(target = "contractEntities", ignore = true)
    void updateEntity(ArtistDTO artistDTO, @MappingTarget ArtistEntity artistEntity);

    @Named("contractEntitiesToIntegers")
    default List<Integer> contractEntitiesToStrings(List<ContractEntity> contractEntities) {
        if (contractEntities != null){
            return contractEntities.stream().map(ContractEntity::getContractId).collect(Collectors.toList());
    }
        return null;
    }

}
