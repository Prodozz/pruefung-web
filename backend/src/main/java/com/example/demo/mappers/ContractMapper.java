package com.example.demo.mappers;

import com.example.demo.DTOs.ContractDTO;
import com.example.demo.entities.ContractEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ContractMapper {

    ContractMapper CONTRACT_MAPPER = Mappers.getMapper(ContractMapper.class);

    @Mapping(target = "artistId",source = "artistEntity.artist.artistId")
    ContractDTO toDto(ContractEntity artistEntity);

    @Mapping(target = "artist", ignore = true)
    ContractEntity toEntity(ContractDTO artistDTO);

    @Mapping(target = "artist", ignore = true)
    void updateEntity(ContractDTO contractDTO, @MappingTarget ContractEntity contractEntity);

}
