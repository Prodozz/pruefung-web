package com.example.demo.repositories;

import com.example.demo.entities.ArtistEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArtistCRUDRepository extends CrudRepository<ArtistEntity, Integer> {
}
