package com.example.demo.repositories;

import com.example.demo.entities.ContractEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractCRUDRepository extends CrudRepository<ContractEntity, Integer> {


}
