package com.example.demo.services;

import com.example.demo.DTOs.ArtistDTO;
import com.example.demo.entities.ArtistEntity;
import com.example.demo.entities.ContractEntity;
import com.example.demo.mappers.ArtistMapper;
import com.example.demo.repositories.ArtistCRUDRepository;
import com.example.demo.repositories.ContractCRUDRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ArtistService {

    ArtistMapper artistMapper;
    ArtistCRUDRepository artistCRUDRepository;
    ContractCRUDRepository contractCRUDRepository;

    public List<ArtistDTO> getAllArtists() {

        List<ArtistEntity> artistEntities = (List<ArtistEntity>) artistCRUDRepository.findAll();
        return artistEntities.stream().map(artistMapper::toDto).collect(Collectors.toList());

    }

    public ArtistDTO getArtist(Integer artistId) {

        ArtistEntity artistEntity = artistCRUDRepository.findById(artistId).get();
        return artistMapper.toDto(artistEntity);

    }

    public void createArtist(ArtistDTO artistDTO) {

        ArtistEntity artistEntity = artistMapper.toEntity(artistDTO);

        List<ContractEntity> contractEntities = null;

        if (artistDTO.getContractEntitiesIds() != null && !artistDTO.getContractEntitiesIds().isEmpty()) {
            contractEntities = artistDTO.getContractEntitiesIds().stream()
                    .map(i -> contractCRUDRepository.findById(i).get()).collect(Collectors.toList());
        }

        artistEntity.setContractEntities(contractEntities);

        artistCRUDRepository.save(artistEntity);

    }

    public void deleteArtist(Integer artistId) {

        ArtistEntity artistEntity = artistCRUDRepository.findById(artistId).get();

        if (artistEntity.getContractEntities() != null && !artistEntity.getContractEntities().isEmpty()) {
            for (ContractEntity contractEntity : artistEntity.getContractEntities()) {
                contractEntity.setArtist(null);
                contractCRUDRepository.save(contractEntity);
            }

        }
            artistCRUDRepository.deleteById(artistId);
    }

    public void updateArtist(ArtistDTO artistDTO) {

        ArtistEntity artistEntity = artistCRUDRepository.findById(artistDTO.getArtistId()).get();
        artistMapper.updateEntity(artistDTO, artistEntity);

        List<ContractEntity> contractEntities = null;

        if (artistDTO.getContractEntitiesIds() != null && !artistDTO.getContractEntitiesIds().isEmpty()) {
            contractEntities = artistDTO.getContractEntitiesIds().stream()
                    .map(i -> contractCRUDRepository.findById(i).get()).collect(Collectors.toList());
        }


        artistCRUDRepository.save(artistEntity);

    }


}

