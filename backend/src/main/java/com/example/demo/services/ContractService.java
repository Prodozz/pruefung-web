package com.example.demo.services;

import com.example.demo.DTOs.ContractDTO;
import com.example.demo.entities.ArtistEntity;
import com.example.demo.entities.ContractEntity;
import com.example.demo.mappers.ContractMapper;
import com.example.demo.repositories.ArtistCRUDRepository;
import com.example.demo.repositories.ContractCRUDRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ContractService {

    ContractMapper contractMapper;
    ContractCRUDRepository contractCRUDRepository;
    ArtistCRUDRepository artistCRUDRepository;

    public List<ContractDTO> getAllContracts() {

        List<ContractEntity> contractEntities = (List<ContractEntity>) contractCRUDRepository.findAll();
        return contractEntities.stream().map(contractMapper::toDto).collect(Collectors.toList());

    }

    public ContractDTO getContract(Integer contractId) {

        ContractEntity contractEntity = contractCRUDRepository.findById(contractId).get();
        return contractMapper.toDto(contractEntity);

    }

    public void createContract(ContractDTO contractDTO) {

        ContractEntity contractEntity = contractMapper.CONTRACT_MAPPER.toEntity(contractDTO);

        ArtistEntity artist = null;

        if (contractDTO.getArtistId() != null ) {
        artist = artistCRUDRepository.findById(contractDTO.getArtistId()).get();
        }

        contractEntity.setArtist(artist);

        contractCRUDRepository.save(contractEntity);

    }

    public void deleteContract(Integer contractId) {

        ContractEntity contractEntity = contractCRUDRepository.findById(contractId).get();

        if (contractEntity.getArtist() != null){
            ArtistEntity artist = artistCRUDRepository.findById(contractEntity.getArtist().getArtistId()).get();
            artist.setContractEntities(artist.getContractEntities().stream()
                    .filter(contract -> !contract.equals(contractEntity)).collect(Collectors.toList()));
            artistCRUDRepository.save(artist);
        }

        contractCRUDRepository.deleteById(contractId);

    }

    public void updateContract(ContractDTO contractDTO) {

        ContractEntity contractEntity = contractCRUDRepository.findById(contractDTO.getContractId()).get();
        contractMapper.updateEntity(contractDTO, contractEntity);

        ArtistEntity artist = null;

        if (contractDTO.getArtistId() != null){
            artist = artistCRUDRepository.findById(contractDTO.getArtistId()).get();
        }

            contractEntity.setArtist(artist);

        contractCRUDRepository.save(contractEntity);

    }



}
