import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/contracts',
      name: 'contracts',
      component: () => import("@/views/ContractsView.vue")
    },
    {
      path: '/',
      name: 'artists',
      component: () => import("@/views/ArtistsView.vue")
    }
  ]
})

export default router
