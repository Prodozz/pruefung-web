import { defineStore } from "pinia";
import axios, { all } from "axios";
import { computed, ref } from "vue";

export const useArtistStore = defineStore("artists", () => {

    const artistObjectKeys = computed(() => {
        if (allArtists.value.length !== 0){
            return  Object.keys(allArtists.value[0])
        }
    })
    const allArtists = ref([]);
    const artist = ref([]);
    const artistsInList = ref([]);


    const getAllArtists = async () => {
        try {
            const artistsApi = await axios.get("http://localhost:8080/api/artists");
            allArtists.value = artistsApi.data;
        } catch (e) {
            console.error(e);
        }
    };

    const getArtist = async (artistId) => {
        try {
            const artistApi = await axios.get("http://localhost:8080/api/artists/" + artistId);
            artist.value = artistApi.data;
        } catch (e) {
            console.error(e);
        }
    };

    const getArtistsInList = async (artistsList) => {

        const listInBetween = ref([]);

        for (let artist in artistsList) {
            try {
                artist = parseInt(artist) + 1;
                const artistApi = await axios.get("http://localhost:8080/api/artists/" + artist);
                listInBetween.value.push(artistApi.data);
            } catch (e) {
                console.error(e);
            }

            artistsInList.value = listInBetween.value;

        }
    };

    const postArtist = async (artist) => {

        await axios.post("http://localhost:8080/api/artists", artist)
            .then((response) => {
                console.log(response);
                getAllArtists();
            }).catch(e => {
                console.error(e);
            });

    };

    const deleteArtist = async (artistId) => {
        try {
            await axios.delete("http://localhost:8080/api/artists/" + artistId)
            allArtists.value.splice(allArtists.value.indexOf(st => st.id === artistId), 1);

        } catch (e) {
            console.error(e)
        }
    };

    const updateArtist = async (thisArtist) => {
        try {
            await axios.put("http://localhost:8080/api/artists" , thisArtist);
            await getAllArtists()

        } catch (e) {
            console.error(e);
        }
    };





    return {
        getAllArtists,
        deleteArtist,
        getArtistsInList,
        getArtist,
        postArtist,
        updateArtist,
        allArtists,
        artist,
        artistsInList,
        artistObjectKeys
    };
});


