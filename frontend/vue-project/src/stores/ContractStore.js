import { defineStore } from "pinia";
import axios, { all } from "axios";
import { computed, ref } from "vue";

export const useContractStore = defineStore("contracts", () => {

    const contractObjectKeys = computed(() => {
        if (allContracts.value.length !== 0){
            return  Object.keys(allContracts.value[0])
        }
    })
    const allContracts = ref([]);
    const contract = ref([]);
    const contractsInList = ref([]);


    const getAllContracts = async () => {
        try {
            const contractsApi = await axios.get("http://localhost:8080/api/contracts");
            allContracts.value = contractsApi.data;
        } catch (e) {
            console.error(e);
        }
    };

    const getContract = async (contractId) => {
        try {
            const contractApi = await axios.get("http://localhost:8080/api/contracts/" + contractId);
            contract.value = contractApi.data;
        } catch (e) {
            console.error(e);
        }
    };

    const getContractsInList = async (contractsList) => {

        const listInBetween = ref([]);

        for (let contract in contractsList) {
            try {
                contract = parseInt(contract) + 1;
                const contractApi = await axios.get("http://localhost:8080/api/contracts/" + contract);
                listInBetween.value.push(contractApi.data);
            } catch (e) {
                console.error(e);
            }

            contractsInList.value = listInBetween.value;

        }
    };

    const postContract = async (contract) => {

        await axios.post("http://localhost:8080/api/contracts", contract)
            .then((response) => {
                console.log(response);
                getAllContracts();
            }).catch(e => {
                console.error(e);
            });

    };

    const deleteContract = async (contractId) => {
        try {
            await axios.delete("http://localhost:8080/api/contracts/" + contractId)
            allContracts.value.splice(allContracts.value.indexOf(st => st.id === contractId), 1);

        } catch (e) {
            console.error(e)
        }
    };

    const updateContract = async (thisContract) => {
        try {
            await axios.put("http://localhost:8080/api/contracts" , thisContract);
            await getAllContracts()

        } catch (e) {
            console.error(e);
        }
    };





    return {
        getAllContracts,
        deleteContract,
        getContractsInList,
        getContract,
        postContract,
        updateContract,
        allContracts,
        contract,
        contractsInList,
        contractObjectKeys
    };
});


